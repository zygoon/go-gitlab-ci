# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Zygmunt Krynicki
FROM golang:latest AS build
ENV GOMODCACHE=/cache/go/pkg/mod
ENV GOCACHE=/cache/go-build
ENV GOBIN=/usr/local/bin
RUN go install github.com/bombsimon/wsl/v3/...@latest
RUN go install github.com/boumenot/gocover-cobertura@latest
RUN go install github.com/gordonklaus/ineffassign@latest
RUN go install github.com/jgautheron/goconst/cmd/goconst@latest
RUN go install github.com/kisielk/errcheck@latest
RUN go install honnef.co/go/tools/cmd/staticcheck@master
RUN ls -l /usr/local/bin

FROM golang:latest
WORKDIR /usr/local/bin
COPY --from=build /usr/local/bin/wsl .
COPY --from=build /usr/local/bin/gocover-cobertura .
COPY --from=build /usr/local/bin/ineffassign .
COPY --from=build /usr/local/bin/goconst .
COPY --from=build /usr/local/bin/errcheck .
COPY --from=build /usr/local/bin/staticcheck .
WORKDIR /src
