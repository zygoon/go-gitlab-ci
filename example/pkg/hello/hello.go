// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package hello

import (
	"fmt"
)

// World prints a welcome message to stdout.
func World() {
	fmt.Printf("Hello World")
}
