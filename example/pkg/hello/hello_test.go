// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package hello_test

import (
	"example.org/go-example/pkg/hello"
)

func ExampleWorld() {
	hello.World()
	// Output: Hello World
}
