// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package main

import (
	"example.org/go-example/pkg/hello"
)

func main() {
	hello.World()
}
