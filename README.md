<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About this repository

This repository contains opinionated selection of GitLab continuous
integration templates for Go.

## Pre-release software warning

This repository has been created recently and does not yet offer a reliable,
stable interface.

## Contributions

Contributions are welcome. Please file issues to discuss new features and
ideas. For smaller things go ahead and propose a pull request directly. 

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making it
easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.
